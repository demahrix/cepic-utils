import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class MadeByCepic extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Made with </> by",
          style: TextStyle(
            fontFamily: "Roboto-Mono",
            package: "cepic",
            fontWeight: FontWeight.w600
          ),
        ),
        Image.asset("assets/logo.png", package: "cepic", height: 80.0)
      ],
    );
  }
}
